section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global pars_int
global string_copy
global print_err
global print_stdout_string
; Передаётся указатель на строку ошибки, вывод строки

print_err:
	mov r9 , 2
	jmp print_string
	
 
; Передаётся код возврата, конец текущего процесса
exit: 
	mov rax, 60
	syscall





string_length:
 xor rax, rax	; очистка rax, чтобы в нем гарантированно был нуль
	.loop:
		cmp byte[rdi+rax], 0	;сравниваем данный символ с нуль-терминатаром
		je .end			;если нуль-терминатор, то выходим
		inc rax			;инкремент rax для проверки следующего символа
		jmp .loop
	.end:
		ret	


print_stdout_string:
	mov r9, 1

; Передаётся указатель на нуль-терминированную строку, выводим её в stdout
print_string:
	call string_length	; в rax - длина строчки	
	mov rdx, rax		; в rdx - длину строчки
	mov rsi, rdi		; в rsi - адрес начала строчки	
	mov rdi, r9		; в rdi - дескриптор потока файла потока вывода			
	mov rax, 1		; в rax - номер сисколла write			
	syscall
	xor rax, rax
	ret

; Переводим строку (выводим символ с кодом 0xA)
print_newline:
	mov rdi , 10
	

; Передаём код символа и выводим его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret



; Выводим беззнаковое 8-байтовое число в десятичном формате
print_uint:
	mov r9, rsp ;в r9 - адрес вершины стека
	mov rax, rdi ;в rax - число, которое нужно перевести
	mov rcx, 10  ;rcx - в качестве делителя
	dec rsp;
	mov byte[rsp], 0
.loop:
	xor rdx, rdx
	div rcx ; делим rax на 10, в rdx - остаток от деления
	add rdx, 0x30 ; переводим остаток в ascii
	dec rsp
	mov byte[rsp], dl ; записываем в стек
	test rax, rax ; проверяем, конец ли числа
	jnz .loop

	mov rdi, rsp
	push r9
	call print_string
	pop r9
	mov rsp, r9
	ret



; Выводим знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns .uint
	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi
.uint:
	call print_uint
	ret


; Передаётся два указателя на нуль-терминированные строки, возвращаем 1 если они равны, 0 иначе
string_equals:

; в rdi - указатель на первую строку, в rsi - на вторую

	call string_length ; считаем длину первой строки
	mov r9 , rax	  ; помещаем ее длину в r9
	xchg rdi, rsi	  ; помещаем в rdi указатель на вторую строку
	call string_length ; считаем длину второй строки
	cmp r9, rax	  ; сравниваем длины двух строк , если не равны - на выход, если равны - то сравниваем посимвольно
	jne .notequals
	
.loop:
	xor rax, rax	
	mov al, byte[rdi] ; помещаем в младший байт rax символ
	cmp al, byte[rsi] ; сравниваем символ первой строки с символом второй строки
	jne .notequals    ; если не равны, то выходим
	cmp al, 0	 ; символы равны, достаточно проверить на нуль-терминатор только один из них
	je .end	         ; если нуль-терминатор, то мы проверили две строки, они равны - выходим и возвращаем 1
	inc rdi           ; инкрементируем rdi, читаем следующий символ
	inc rsi		 ; также с rsi
	jmp .loop

.notequals:
	xor rax, rax
	ret	
.end:
	mov rax, 1
	ret
	
    

; Читаем один символ из stdin и возвращаем его. 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
	mov rdx, 1 ;длина 
	push 0	  ; резервируем место для чтения
	mov rsi, rsp ; адрес, куда будет считываться символ
	syscall
	pop rax	     ; считываем в rax введенный символ
	ret	
    
; Функция дописывает к слову нуль-терминатор

read_word:
	push r13 ;пушим calle-saved регистры
	push r14 
	push r15
	mov r13, rdi ;помещаем в r13 адрес начала буфера
	mov r14, rsi ;помещаем в r14 размер буфера 
	xor r15, r15 ;счетчик сколько символов прочитали

.first_read:
	call read_char
	cmp rax, 0x20 ;проверка на пробельный символ
	je .first_read
	cmp rax, 0x9  ;проверка на пробельный символ
	je .first_read
	cmp rax, 0xA  ;проверка на пробельный символ
	je .first_read
	cmp rax, 0x0  ;если первый же символ - нуль терминатор
	je .end
	
.continue_read:
	cmp r14, r15 ;проверка, есть ли запас буфера
	jbe .overflow
	mov byte[r13+r15], al ;записываем в буффер первый символ
	cmp al, 0x20
	je .end
	cmp al, 0x9
	je .end
	cmp al, 0xA
	je .end
	cmp al, 0x0
	je .end
	inc r15
	cmp r15, r14
	je .overflow
	call read_char 
	jmp .continue_read

.overflow:
	xor rax, rax
	pop r15
	pop r14
	pop r13
	ret
.end:
	mov byte[r13+r15] , 0
	mov rax, r13
	mov rdx, r15
	pop r15
	pop r14
	pop r13
	ret	
		 

; Передаём указатель на строку, пытаем
; прочитать из её начала беззнаковое число.
; Возвращаем в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor r10, r10			
	mov rcx, 10			; 10 в rcx используем в качестве делителя
	xor rax, rax
	.loop:
		xor r11, r11
		mov r11b, byte[rdi + r10]
		cmp r11b, 0x39		
		ja .end			
		cmp r11b, 0x30			
		jb .end					
		inc r10			
		sub r11b, '0'		
		mul rcx			
		add rax, r11		
		jmp .loop
	.end:
		mov rdx, r10		
		ret




; Передаём указатель на строку, пытаем
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращаем в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'		
	jne .uint			
	inc rdi				
	call parse_uint			
	test rdx, rdx			
	jz .not_digit			
	neg rax				
	inc rdx				
	ret
	.uint:
		jmp parse_uint		
		ret
	.not_digit:
		xor rax, rax		
		ret


; Передаём указатель на строку, указатель на буфер и длину буфера
; Копируем строку в буфер
; Возвращаем длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length ; в rax храним длину строки
	inc rax
	pop rdx
	pop rsi
	pop rdi
	cmp rax , rdx
	jg .less
	xor r11, r11
.loop:
	mov r11b, byte[rdi]
	mov byte[rsi], r11b
	cmp r11b, 0
	je .end
	inc rdi
	inc rsi
	jmp .loop
	
.less:
	xor rax, rax
	
.end:
	ret
	
	
