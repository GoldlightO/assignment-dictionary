global find_word
%include "lib.inc"

section .text

;rdi - null pointer string - key link
;rsi - begining of the map link
;return adress of key , if success
;else returns 0

find_word:
	xor rax,rax
.loop:
	cmp rsi, 0 ;проверка ссылки 
	je .end
	push rdi
	push rsi
	add rsi, 8 ;смещение указателя на ключ
	call string_equals
	pop rsi
	pop rdi
	cmp rax, 1
	je .success
	mov rsi, [rsi] ; занесение в rsi ссылки на след элемент в словаре
	jmp .loop
.success:
	mov rax, rsi ; занесение в rsi ссылки на найденный ключ
	ret
.end:
	xor rax, rax
	ret
	
