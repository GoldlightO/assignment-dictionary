section .text
%include "colon.inc"
%include "lib.inc"

section .rodata
%include "words.inc"
err: db "The size of string is greater than buffer size",0
err2: db "No word",0

section .text

extern find_word


global _start
_start:
	sub rsp,256 ; освобождаем место под буффер
	mov rdi, rsp ; в rdi - адрес на начало буфера для функции read_word
	mov rsi, 256 ;длина буфера для функции read_word
	call read_word
	cmp rax, 0 ; проверка на длинну ввода строки
	je .overflow
	mov rsi , first
	mov rdi, rax
	call find_word
	add rsp, 256 ; приведение стека к начальному состоянию
	cmp rax, 0 ; проверка на нахождение слова
	je .no_word	
	add rax, 8 ; смещение указателя на ключ 
	push rax ; занесение адреса ключа в стек
	mov rdi, [rsp] ; подготовка к вызову string_length - законсение в rdi адрес ключа
	call string_length
	pop rdi; забираем адрес ключа из стека в rdi - подготовка к вызову print_string
	add rdi, rax ;смещение указателя на конец ключа
	inc rdi; учитываем нуль-терминатор, в rdi - ссылка на значение
	call print_stdout_string
	
.end:
	call print_newline
	xor rdi, rdi
	call exit
	

.overflow:
	add rsp, 256
	mov rdi, err
	call print_err
	jmp .end

.no_word:
	mov rdi, err2
	call print_err
	jmp .end
